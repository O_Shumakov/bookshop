﻿using BookShop.Entities;
using BookShop.ViewModels.Authors;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.Books
{
    public class BookView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int TotalPages { get; set; }
        public int Rating { get; set; }
        public string YearOfPublishing { get; set; }
        public List<AuthorView> Authors { get; set; }

        public BookView()
        { }

        public BookView(string title, int totalPages, int rating, string year, List<AuthorView> authors = null)
        {
            Title = title;
            TotalPages = totalPages;
            Rating = rating;
            YearOfPublishing = year;
            Authors = new List<AuthorView>(authors);
            if (authors != null)
            {
                Authors = authors;
            }
        }
    }
}
