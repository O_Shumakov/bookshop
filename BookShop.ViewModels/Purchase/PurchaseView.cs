﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.Purchase
{
    public class PurchaseView
    {
        public string UserId { get; set; }
        public string PublicationId { get; set; }

        public PurchaseView()
        {
        }

        public PurchaseView(string userId, string publicationId)
        {
            UserId = userId;
            PublicationId = publicationId;
        }
    }
}
