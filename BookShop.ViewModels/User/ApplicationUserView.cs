﻿using BookShop.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.User
{
    public class ApplicationUserView
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Age { get; set; }

        public ApplicationUserView()
        { }

        public ApplicationUserView(string id, string email, string firsName, string lastName, string age, Gender gender)
        {
            UserId = id;
            Email = email;
            FirstName = firsName;
            LastName = lastName;
            Age = age;
            Gender = gender;
        }
    }
}
