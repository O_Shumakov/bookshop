﻿using BookShop.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.ResponseViews
{
    public class RegisterResponseAccountView
    {
        public ApplicationUser User { get; set; }
        public string Token { get; set; }
        public List<string> Messages { get; set; }

        public RegisterResponseAccountView()
        {
            Messages = new List<string>();
        }
    }
}
