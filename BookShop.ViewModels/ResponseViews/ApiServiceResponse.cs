﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.ResponseViews
{
    public class ApiServiceResponse<T> where T : new()
    {
        public bool Succeed { get; set; }
        public T ResponseObject { get; set; }
        public string Errors { get; set; }

        public ApiServiceResponse()
        {
            Succeed = false;
            Errors = "BadRequest";
        }

        public ApiServiceResponse(bool succeed, string errors)
        {
            Succeed = succeed;
            Errors = errors;
        }
    }
}
