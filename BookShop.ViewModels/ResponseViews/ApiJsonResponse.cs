﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.ResponseViews
{
    public class ApiJsonResponse
    {
        public bool Succeed { get; set; }
        public string ResponseString { get; set; }
        public string Errors { get; set; }

        public ApiJsonResponse()
        {
            Succeed = false;
            Errors = "BadRequest";
            ResponseString = string.Empty;
        }

        public ApiJsonResponse(bool succeed, string errors)
        {
            Succeed = succeed;
            Errors = errors;
        }
    }
}
