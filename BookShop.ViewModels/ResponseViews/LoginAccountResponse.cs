﻿using BookShop.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.ResponseViews
{
    public class LoginAccountResponse
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        public Gender Gender { get; set; }
        public string Token { get; set; }
        public RoleType RoleType { get; set; }
    }
}
