﻿using BookShop.Entities;
using BookShop.ViewModels.Authors;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.Journals
{
    public class JournalView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int TotalPages { get; set; }
        public int Rating { get; set; }
        public string Publisher { get; set; }
        public List<AuthorView> Authors { get; set; }

        public JournalView()
        { }

        public JournalView(string title, int totalPages, int rating, string publisher, List<AuthorView> authors = null)
        {
            Title = title;
            TotalPages = totalPages;
            Rating = rating;
            Publisher = publisher;
            Authors = new List<AuthorView>(authors);
            if (authors != null)
            {
                Authors = authors;
            }
        }
    }
}
