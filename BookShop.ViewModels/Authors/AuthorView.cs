﻿using BookShop.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.ViewModels.Authors
{
    public class AuthorView
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Gender Gender { get; set; }

        public AuthorView()
        { }

        public AuthorView(string email, string firstName, string lastName, Gender gender)
        {
            Email = email;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
        }
    }
}
