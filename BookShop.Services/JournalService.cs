﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories;
using BookShop.Entities;
using BookShop.Services.Common;
using BookShop.ViewModels.Journals;
using BookShop.ViewModels.ResponseViews;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Services
{
    public class JournalService
    {
        private readonly JournalRepository _journalRepository;
        private readonly JournalAuthorRepository _journalAuthorRepository;

        public JournalService(ApplicationContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _journalRepository = new JournalRepository(context);
            _journalAuthorRepository = new JournalAuthorRepository(context);

        }

        public async Task<ApiServiceResponse<JournalView>> CreateJournal(JournalView journalView, List<string> authorIds)
        {
            var result = new ApiServiceResponse<JournalView>(false, Constants.UnableCreateJournal);
            if (await FindJournal(journalView))
            {
                result.Errors = Constants.JournalExist;
                return result;
            }
            var journal = new Journal(journalView.Title, journalView.TotalPages, journalView.Rating, journalView.Publisher);
            journal.Id = Guid.NewGuid().ToString();
            var addjournalResult = await _journalRepository.Create(journal);
            if (addjournalResult == null || addjournalResult.State != Microsoft.EntityFrameworkCore.EntityState.Added)
            {
                result.Succeed = false;
                result.ResponseObject = null;
                result.Errors = Constants.UnableCreateJournal;
                return result;
            }
            var journalAuthorResult = await AddJournalAuthors(journal.Id, authorIds);
            journalView.Id = journal.Id;
            result.Errors = journalAuthorResult.Succeed ? string.Empty : journalAuthorResult.Errors;
            result.Succeed = true;
            result.ResponseObject = journalView;
            return result;
        }

        public async Task<ApiServiceResponse<int>> AddJournalAuthors(string journalId, List<string> authorIds)
        {
            var result = new ApiServiceResponse<int>(false, Constants.UnableAddAuthors);
            result.ResponseObject = 0;
            if (string.IsNullOrEmpty(journalId))
            {
                return result;
            }
            if (authorIds != null && authorIds.Count > 0)
            {
                var journalAuthorList = new List<JournalAuthor>();
                foreach (var authorId in authorIds)
                {
                    var checkResult = await _journalAuthorRepository.GetJournalAuthorByIds(journalId, authorId);
                    if (checkResult != null)
                    {
                        continue;
                    }
                    journalAuthorList.Add(new JournalAuthor(journalId, authorId));
                }
                if (journalAuthorList.Count > 0)
                {
                    await _journalAuthorRepository.Create(journalAuthorList);
                }
                result.Succeed = true;
                result.ResponseObject = 1;
                result.Errors = string.Empty; ;
            }
            return result;
        }

        public async Task<ApiServiceResponse<int>> DeleteJournalAuthors(string journalId, List<string> authorIds, bool removeAll = false)
        {
            var result = new ApiServiceResponse<int>(false, Constants.UnableDeleteAuthors);
            result.ResponseObject = 0;
            if (string.IsNullOrEmpty(journalId))
            {
                return result;
            }
            if (removeAll)
            {
                var allJournalAuthors = _journalAuthorRepository.GetAllAuthorIdsByJournalId(journalId);
                await _journalAuthorRepository.DeleteJournalAuthorsByJournalId(journalId);
                result.Succeed = true;
                result.Errors = string.Empty;
                return result;
            }
            if (authorIds != null && authorIds.Count > 0)
            {
                var journalAuthorList = new List<JournalAuthor>();
                foreach (var authorId in authorIds)
                {
                    var journalAuthor = await _journalAuthorRepository.GetJournalAuthorByIds(journalId, authorId);
                    if (journalAuthor != null)
                    {
                        journalAuthorList.Add(journalAuthor);
                    }
                }
                if (journalAuthorList.Count > 0)
                {
                    await _journalAuthorRepository.Delete(journalAuthorList);
                }
                result.Succeed = true;
                result.ResponseObject = 1;
                result.Errors = string.Empty;
            }
            return result;
        }

        public async Task<ApiServiceResponse<List<JournalView>>> FindJournals(string title = "", int totalPages = -1, int rating = -1, string publisher = "")
        {
            var result = new ApiServiceResponse<List<JournalView>>(false, Constants.UnableFindJournal);
            var allJournals = await _journalRepository.GetAllJournals();
            if (allJournals == null)
            {
                return result;
            }
            if (string.IsNullOrEmpty(title) && allJournals.Count > 0)
            {
                var sortedcCollection = allJournals.Where(x => x.Title == title).ToList();
                allJournals = sortedcCollection;
            }
            if (totalPages > 0 && allJournals.Count > 0)
            {
                var sortedcCollection = allJournals.Where(x => x.TotalPages == totalPages).ToList();
                allJournals = sortedcCollection;
            }
            if (rating >= 0 && allJournals.Count > 0)
            {
                var sortedcCollection = allJournals.Where(x => x.Rating == rating).ToList();
                allJournals = sortedcCollection;
            }
            if (string.IsNullOrEmpty(publisher) && allJournals.Count > 0)
            {
                var sortedcCollection = allJournals.Where(x => x.Publisher == publisher).ToList();
                allJournals = sortedcCollection;
            }
            if (allJournals != null)
            {
                var finishJournalCollection = new List<JournalView>();
                foreach (var journal in allJournals)
                {
                    var journalView = new JournalView(journal.Title, journal.TotalPages, journal.Rating, journal.Publisher);
                    journalView.Id = journal.Id;
                    finishJournalCollection.Add(journalView);
                }
                result.Succeed = true;
                result.Errors = string.Empty;
                result.ResponseObject = finishJournalCollection;
            }
            return result;
        }

        private async Task<bool> FindJournal(JournalView journalView)
        {
            var result = false;
            if (journalView == null)
            {
                return result;
            }
            var searchResult = await _journalRepository.FindJournalByParams(journalView.Title, journalView.TotalPages, journalView.Rating, journalView.Publisher);
            if (searchResult != null)
            {
                result = true;
            }
            return result;
        }

        public async Task<ApiServiceResponse<JournalView>> UpdateJournal(JournalView journalView, List<string> authorIds = null)
        {
            var result = new ApiServiceResponse<JournalView>(false, Constants.UnableUpdateJournal);
            var journal = await _journalRepository.GetJournalById(journalView.Id);
            if (journal == null)
            {
                return result;
            }
            var journalUpdateResult = await _journalRepository.UpdateJournal(journal);
            if (journalUpdateResult == null || journalUpdateResult.State != Microsoft.EntityFrameworkCore.EntityState.Modified)
            {
                result.ResponseObject = null;
                return result;
            }
            result.Errors = string.Empty;
            if (authorIds != null && authorIds.Count > 0)
            {
                var journalAuthorDeleteResult = await DeleteJournalAuthors(journal.Id, authorIds, true);
                var journalAuthorAddResult = await AddJournalAuthors(journal.Id, authorIds);
                result.Errors = journalAuthorDeleteResult.Succeed && journalAuthorAddResult.Succeed ? string.Empty : Constants.UnableUpdateAuthors;
            }

            journalView.Id = journal.Id;
            result.Succeed = true;
            result.ResponseObject = journalView;
            return result;
        }

        public async Task<ApiServiceResponse<int>> DeleteJournal(string journalId)
        {
            var result = new ApiServiceResponse<int>(false, Constants.UnableRemoveJournal);
            var journal = await _journalRepository.GetJournalById(journalId);
            if (journal == null)
            {
                result.Errors = Constants.UnableFindJournal;
                return result;
            }
            var journalDeleteResult = await _journalRepository.DeleteJournal(journal);
            if (journalDeleteResult == null || journalDeleteResult.State != Microsoft.EntityFrameworkCore.EntityState.Deleted)
            {
                result.ResponseObject = 0;
                return result;
            }
            result.Errors = string.Empty;

            var journalAuthorDeleteResult = await DeleteJournalAuthors(journal.Id, null, true);
            result.Errors = journalAuthorDeleteResult.Succeed ? string.Empty : Constants.UnableDeleteAuthors;

            result.Succeed = true;
            result.ResponseObject = 1;
            return result;
        }
    }
}
