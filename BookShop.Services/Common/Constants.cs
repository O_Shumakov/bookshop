﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.Services.Common
{
    public class Constants
    {
        #region Purchase
        public static readonly string UnablePurchase = "Purchase not found";
        public static readonly string UnableCreateAuthor = "Unable to create new author";
        public static readonly string UnableUpdateAuthor = "Unable to update author";
        public static readonly string UnableRemoveAuthor = "Unable to remove author";
        #endregion
        #region Book
        public static readonly string UnableCreateBook = "Unable to create new book";
        public static readonly string UnableUpdateBook = "Unable to update book";
        public static readonly string UnableRemoveBook = "Unable to remove book";
        public static readonly string UnableFindBooks = "Books not found";
        public static readonly string BookExist = "This book already exist.";
        #endregion
        #region Journal
        public static readonly string UnableCreateJournal = "Unable to create new journal";
        public static readonly string UnableUpdateJournal = "Unable to update journal";
        public static readonly string UnableRemoveJournal = "Unable to remove journal";
        public static readonly string UnableFindJournal = "Journals not found";
        public static readonly string JournalExist = "This journal already exist.";
        #endregion
        #region Author
        public static readonly string UnableAddAuthors = "Unable to add authors";
        public static readonly string UnableDeleteAuthors = "Unable to remove authors";
        public static readonly string UnableUpdateAuthors = "Unable to update authors";
        #endregion
        #region API
        public static readonly string PermissionerrorMessage = "You don't have permission.";
        public static readonly string BadRequestMessage = "Some error happen, please try later.";
        #endregion
    }
}
