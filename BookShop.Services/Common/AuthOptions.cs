﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.Services.Common
{
    public class AuthOptions
    {
        public const string ISSUER = "BookShopAuthServer";
        public const string AUDIENCE = "https://10.10.2.7:5566/";
        public const int LIFETIME = 1440; //24*60
        const string KEY = "testSecretKey_Qwe123@";
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
