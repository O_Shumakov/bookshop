﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories;
using BookShop.Entities;
using BookShop.Services.Common;
using BookShop.ViewModels.Authors;
using BookShop.ViewModels.ResponseViews;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Services
{
    public class AuthorService
    {
        private readonly AuthorRepository _authorRepository;

        public AuthorService(ApplicationContext context)
        {
            _authorRepository = new AuthorRepository(context);
        }

        public async Task<ApiServiceResponse<AuthorView>> CreateAuthor(AuthorView authorView)
        {
            var result = new ApiServiceResponse<AuthorView>(true, string.Empty);
            var author = new Author(authorView.Email, authorView.FirstName, authorView.LastName, authorView.Gender);
            author.Id = Guid.NewGuid().ToString();

            var addAuthorResult = await _authorRepository.Create(author);
            if (addAuthorResult == null || addAuthorResult.State != Microsoft.EntityFrameworkCore.EntityState.Added)
            {
                result.Succeed = false;
                result.ResponseObject = null;
                result.Errors = Constants.UnableCreateAuthor;
            }
            authorView.Id = author.Id;
            result.ResponseObject = authorView;
            return result;
        }

        public async Task<ApiServiceResponse<List<string>>> CreateAuthors(List<AuthorView> authorViews)
        {
            var result = new ApiServiceResponse<List<string>>(false, Constants.UnableCreateAuthor);
            result.ResponseObject = new List<string>();
            foreach (var author in authorViews)
            {
               var addAuthorResult = await CreateAuthor(author);
                if (addAuthorResult != null)
                {
                    result.Succeed = addAuthorResult.Succeed;
                    result.ResponseObject.Add(addAuthorResult.ResponseObject == null ? string.Empty : addAuthorResult.ResponseObject.Id);
                    result.Errors = result.Errors;
                }
            }
            return result;
        }

        public async Task<ApiServiceResponse<int>> UpdateAuthor(string authorId, AuthorView authorView)
        {
            var result = new ApiServiceResponse<int>(false, Constants.UnableUpdateAuthor);
            var author = await _authorRepository.GetAuthorById(authorId);
            if (author == null)
            {
                return result;
            }
            if (authorView != null && !string.IsNullOrEmpty(authorView.Email) && !string.IsNullOrEmpty(authorView.FirstName) && !string.IsNullOrEmpty(authorView.LastName))
            {
                author.Email = authorView.Email;
                author.FirstName = authorView.FirstName;
                author.LastName = authorView.LastName;
                author.Gender = authorView.Gender;
            }
            var updateAuthorResult = await _authorRepository.UpdateAuthor(author);
            result.ResponseObject = 0;
            if (updateAuthorResult != null && updateAuthorResult.State == Microsoft.EntityFrameworkCore.EntityState.Modified)
            {
                result.Succeed = true;
                result.ResponseObject = 1;
                result.Errors = string.Empty;
            }
            return result;
        }

        public async Task<ApiServiceResponse<int>> DeleteAuthor(string authorId)
        {
            var result = new ApiServiceResponse<int>(true, string.Empty);
            var response = await _authorRepository.DeleteAuthor(authorId);
            result.ResponseObject = 1;
            if (response == null || response.State != Microsoft.EntityFrameworkCore.EntityState.Deleted)
            {
                result.Succeed = false;
                result.ResponseObject = 0;
                result.Errors = Constants.UnableRemoveAuthor;
            }
            return result;
        }
    }
}
