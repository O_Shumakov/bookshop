﻿using BookShop.DataAccess;
using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories;
using BookShop.Entities;
using BookShop.Entities.Enums;
using BookShop.Services.Common;
using BookShop.ViewModels.ResponseViews;
using BookShop.ViewModels.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Services
{
    public class AccountService
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly AccountRepository _accountRepository;
        private readonly RegularUserRepository _userRepository;

        public AccountService(ApplicationContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _signInManager = signInManager;
            _accountRepository = new AccountRepository(userManager);
            _userRepository = new RegularUserRepository(context);
        }

        public async Task<bool> IsEmailExist(string email)
        {
            bool result = await _accountRepository.GetByEmail(email) == null ? false : true;
            return result;
        }

        public async Task<ApiServiceResponse<ApplicationUserView>> GetApplicationUser(string id)
        {
            var response = new ApiServiceResponse<ApplicationUserView>();
            var result = new ApplicationUserView();
            var user = await _accountRepository.GetUserById(id);

            if (user == null)
            {
                response.Succeed = false;
                response.Errors = "User not found.";
                response.ResponseObject = null;
                return response;
            }

            result.UserId = user.Id;
            result.FirstName = user.FirstName;
            result.LastName = user.LastName;
            result.Gender = user.Gender;
            result.Age = user.Age;
 
            response.ResponseObject = result;
            response.Errors = string.Empty;
            return response;
        }

        public async Task<ApiJsonResponse> Login(LoginView view)
        {
            var response = new ApiJsonResponse(false, "");
            response.Succeed = true;
            response.Errors = null;
            var result = await GenerateJwtToken(view.Email, view.Password, true);
            var user = await _accountRepository.GetByEmail(view.Email);
            if (user != null)
            {
                result.FirstName = user.FirstName;
                result.LastName = user.LastName;
                result.UserId = user.Id;
            }
            if (result.RoleType == RoleType.Admin)
            {
                //for admin role
            }
            if (result.RoleType == RoleType.User)
            {
                var currentUser = await _userRepository.GetUserByUserId(user.Id);
                result.Id = currentUser.Id;
            }
            response.ResponseString = JsonConvert.SerializeObject(result);
            return response;
        }

        public async Task<RegisterResponseAccountView> RegisterUser(RegisterView model)
        {
            var responseModel = new RegisterResponseAccountView();
            var user = new ApplicationUser(model.Email, model.FirstName, model.LastName, model.Gender);
            user.Id = Guid.NewGuid().ToString();
            user.UserName = model.Email;           
            user.Age = model.Age;
            IdentityResult result = await _accountRepository.CreateUserWithPassword(user, model.Password);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    responseModel.Messages.Add(error.Description);
                }
                return responseModel;
            }
            result = await _accountRepository.AddToRole(user, RoleType.User.ToString());
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    responseModel.Messages.Add(error.Description);
                }
                return responseModel;
            }
            var response = await GenerateJwtToken(model.Email, model.Password, true);
            if (string.IsNullOrEmpty(response.Token))
            {
                responseModel.Messages.Add(AuthNotify.IsNotExist);
                return responseModel;
            }
            user = await _accountRepository.GetByEmail(model.Email);
            responseModel.User = user;
            responseModel.Token = response.Token;
            return responseModel;
        }

        private async Task<IdentityResult> CreateUserWithExternalProvider(ApplicationUser user)
        {
            IdentityResult result = await _accountRepository.CreateUser(user);
            if (!result.Succeeded)
            {
                return result;
            }
            result = await _accountRepository.AddToRole(user, RoleType.User.ToString());
            return result;
        }

        private async Task<LoginAccountResponse> GenerateJwtToken(string email, string password, bool withPassword)
        {
            ClaimsIdentity identity = await GetIdentity(email, password, withPassword);
            if (identity == null)
            {
                return null;
            }
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var result = new LoginAccountResponse();
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            result.Token = encodedJwt;
            var role = identity.Claims.LastOrDefault()?.Value;
            if (role != null)
            {
                RoleType temporaryRole;
                Enum.TryParse(role, out temporaryRole);
                result.RoleType = temporaryRole;
            }

            return result;
        }

        private async Task<ClaimsIdentity> GetIdentity(string email, string password, bool withPassword)
        {
            email = email?.ToLower();
            //if (withPassword)
            //{
            //    var signIn = await _signInManager.PasswordSignInAsync(email, password, true, lockoutOnFailure: true);
            //    if (!signIn.Succeeded)
            //    {
            //        return null;
            //    }
            //}
            var user = await _accountRepository.GetByEmail(email);
            if (user == null)
            {
                return null;
            }
            var role = await _accountRepository.GetRoles(user);
            var claims = new List<Claim>
            {
                new Claim("Id", user.Id),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
            };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
