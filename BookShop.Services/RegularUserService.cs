﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories;
using BookShop.Entities;
using BookShop.ViewModels.ResponseViews;
using BookShop.ViewModels.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Services
{
    public class RegularUserService
    {
        private readonly AccountService _accountService;
        private readonly RegularUserRepository _regularUserRepository;
        private readonly UserBookRepository _userBookRepository;
        private readonly UserJournalRepository _userJournalRepository;
        private readonly UserManager<ApplicationUser> _userManager;

        public RegularUserService(ApplicationContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _regularUserRepository = new RegularUserRepository(context);
            _userBookRepository = new UserBookRepository(context);
            _userJournalRepository = new UserJournalRepository(context);
        }

        public async Task<int> Count()
        {
            var count = await _regularUserRepository.Count();
            return count;
        }

        public async Task<ApiServiceResponse<int>> Create(RegisterView registrModel)
        {
            var result = new ApiServiceResponse<int>(true, string.Empty);
            var userResponse = await _accountService.RegisterUser(registrModel);
            if (userResponse == null || userResponse.Messages.Count != 0 || userResponse.User == null)
            {
                result.Succeed = false;
                result.ResponseObject = 0;
                result.Errors = userResponse.Messages.FirstOrDefault();
                return result;
            }

            var regularUser = new RegularUser();
            regularUser.Id = Guid.NewGuid().ToString();
            regularUser.UserId = userResponse.User.Id;

            await _regularUserRepository.Create(regularUser);
            result.ResponseObject = 1;
            return result;
        }

        public async Task<string> GetRegularUserIdByUserId(string userId)
        {
            var regularUserId = await _regularUserRepository.GetUserByUserId(userId);
            return regularUserId == null ? null : regularUserId.Id;
        }

        public async Task<ApiServiceResponse<RegularUser>> GetRegularUserByUserId(string userId)
        {
            var result = new ApiServiceResponse<RegularUser>(false, "User not found.");
            var regularUser = await _regularUserRepository.GetUserByUserId(userId);
            if (regularUser != null)
            {
                result.Succeed = true;
                result.Errors = string.Empty;
                result.ResponseObject = regularUser;
            }
            return result;
        }

        public async Task<ApiServiceResponse<bool>> Update(ApplicationUserView userView)
        {
            var result = new ApiServiceResponse<bool>();
            result.Errors = string.Empty;
            var user = await _regularUserRepository.GetUserByUserId(userView.UserId);

            if (userView != null && (string.IsNullOrEmpty(userView.Age) ||
                string.IsNullOrEmpty(userView.Email) ||
                string.IsNullOrEmpty(userView.FirstName) ||
                string.IsNullOrEmpty(userView.LastName)))
            {
                result.Errors = "Enter required fields";
            }
            if (string.IsNullOrEmpty(result.Errors))
            {
                user.User.Email = userView.Email;
                user.User.FirstName = userView.FirstName;
                user.User.LastName = userView.LastName;
            }
            await _regularUserRepository.UpdateUser(user);

            result.Succeed = true;
            result.Errors = string.Empty;
            return result;
        }

        public async Task DeleteRegularUser(string userId)
        {
            var user = await _regularUserRepository.GetUserByUserId(userId);
            if (user != null)
            {
                await _userBookRepository.DeleteUserBooksByUserId(user.UserId);
                await _userJournalRepository.DeleteUserJournalsByUserId(user.UserId);
                await _regularUserRepository.Delete(user.Id);
                await _userManager.DeleteAsync(user.User);
            }
        }


    }
}
