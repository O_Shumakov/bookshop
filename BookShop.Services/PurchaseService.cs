﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories;
using BookShop.Entities;
using BookShop.Services.Common;
using BookShop.ViewModels.Purchase;
using BookShop.ViewModels.ResponseViews;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Services
{
    public class PurchaseService
    {
        private readonly RegularUserRepository _regularUserRepository;
        private readonly UserBookRepository _userBookRepository;
        private readonly UserJournalRepository _userJournalRepository;
        private readonly BookRepository _bookRepository;
        private readonly JournalRepository _journalRepository;

        public PurchaseService(ApplicationContext context)
        {
            _regularUserRepository = new RegularUserRepository(context);
            _userBookRepository = new UserBookRepository(context);
            _userJournalRepository = new UserJournalRepository(context);
            _bookRepository = new BookRepository(context);
            _journalRepository = new JournalRepository(context);
        }

        public async Task<ApiServiceResponse<int>> AddBookPurchase(PurchaseView purchaseView)
        {
            var serviceResponse = new ApiServiceResponse<int>(false, "Unable to create purchase");
            var user = await _regularUserRepository.GetUserByUserId(purchaseView.UserId);
            var book = await _bookRepository.GetBookById(purchaseView.PublicationId);
            if (user == null || book == null)
            {
                return serviceResponse;
            }
            var result = await _userBookRepository.Create(new UserBook(user.UserId, book.Id));
            if (result.State == EntityState.Added)
            {
                serviceResponse.Succeed = true;
                serviceResponse.ResponseObject = 1;
                serviceResponse.Errors = string.Empty;
            }
            return serviceResponse;
        }

        public async Task<ApiServiceResponse<int>> AddJournalPurchase(PurchaseView purchaseView)
        {
            var serviceResponse = new ApiServiceResponse<int>(false, "Unable to create purchase");
            var user = await _regularUserRepository.GetUserByUserId(purchaseView.UserId);
            var journal = await _journalRepository.GetJournalById(purchaseView.PublicationId);
            if (user == null || journal == null)
            {
                return serviceResponse;
            }
            var result = await _userJournalRepository.Create(new UserJournal(user.UserId, journal.Id));
            if (result.State == EntityState.Added)
            {
                serviceResponse.Succeed = true;
                serviceResponse.ResponseObject = 1;
                serviceResponse.Errors = string.Empty;
            }
            return serviceResponse;
        }

        public async Task<ApiServiceResponse<int>> DeleteBookPurchase(PurchaseView purchaseView)
        {
            var serviceResponse = new ApiServiceResponse<int>(false, Constants.UnablePurchase);
            var user = await _regularUserRepository.GetUserByUserId(purchaseView.UserId);
            var book = await _bookRepository.GetBookById(purchaseView.PublicationId);
            if (user == null || book == null)
            {
                return serviceResponse;
            }
            var result = await _userBookRepository.DeleteUserBookByUserId(user.UserId, book.Id);
            if (result.State == EntityState.Deleted)
            {
                serviceResponse.Succeed = true;
                serviceResponse.ResponseObject = 1;
                serviceResponse.Errors = string.Empty;
            }
            return serviceResponse;
        }

        public async Task<ApiServiceResponse<int>> DeleteJournalPurchase(PurchaseView purchaseView)
        {
            var serviceResponse = new ApiServiceResponse<int>(false, Constants.UnablePurchase);
            var user = await _regularUserRepository.GetUserByUserId(purchaseView.UserId);
            var journal = await _journalRepository.GetJournalById(purchaseView.PublicationId);
            if (user == null || journal == null)
            {
                return serviceResponse;
            }
            var result = await _userJournalRepository.DeleteUserJournalByUserId(user.UserId, journal.Id);
            if (result.State == EntityState.Deleted)
            {
                serviceResponse.Succeed = true;
                serviceResponse.ResponseObject = 1;
                serviceResponse.Errors = string.Empty;
            }
            return serviceResponse;
        }
    }
}
