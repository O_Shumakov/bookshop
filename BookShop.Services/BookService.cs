﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories;
using BookShop.Entities;
using BookShop.Services.Common;
using BookShop.ViewModels.Authors;
using BookShop.ViewModels.Books;
using BookShop.ViewModels.ResponseViews;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.Services
{
    public class BookService
    {
        private readonly BookRepository _bookRepository;
        private readonly BookAuthorRepository _bookAuthorRepository;

        public BookService(ApplicationContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _bookRepository = new BookRepository(context);
            _bookAuthorRepository = new BookAuthorRepository(context);
        }

        public async Task<ApiServiceResponse<BookView>> CreateBook(BookView bookView, List<string> authorIds)
        {
            var result = new ApiServiceResponse<BookView>(false, Constants.UnableCreateBook);
            if (await FindBook(bookView))
            {
                result.Errors = Constants.BookExist;
                return result;
            }
            var book = new Book(bookView.Title, bookView.TotalPages, bookView.Rating, bookView.YearOfPublishing);
            book.Id = Guid.NewGuid().ToString();
            var addBookResult = await _bookRepository.Create(book);
            if (addBookResult == null || addBookResult.State != Microsoft.EntityFrameworkCore.EntityState.Added)
            {
                result.Succeed = false;
                result.ResponseObject = null;
                result.Errors = Constants.UnableCreateBook;
                return result;
            }
            var bookAuthorResult = await AddBookAuthors(book.Id, authorIds);
            bookView.Id = book.Id;
            result.Errors = bookAuthorResult.Succeed ? string.Empty : bookAuthorResult.Errors;
            result.Succeed = true;
            result.ResponseObject = bookView;
            return result;
        }

        public async Task<ApiServiceResponse<int>> AddBookAuthors(string bookId, List<string> authorIds)
        {
            var result = new ApiServiceResponse<int>(false, Constants.UnableAddAuthors);
            result.ResponseObject = 0;
            if (string.IsNullOrEmpty(bookId))
            {
                return result;
            }
            if (authorIds != null && authorIds.Count > 0)
            {
                var bookAuthorList = new List<BookAuthor>();
                foreach (var authorId in authorIds)
                {
                    var checkResult = await _bookAuthorRepository.GetBookAuthorByIds(bookId, authorId);
                    if (checkResult != null)
                    {
                        continue;
                    }
                    bookAuthorList.Add(new BookAuthor(bookId, authorId));
                }
                if (bookAuthorList.Count > 0)
                {
                    await _bookAuthorRepository.Create(bookAuthorList);
                }
                result.Succeed = true;
                result.ResponseObject = 1;
                result.Errors = string.Empty; ;
            }
            return result;
        }

        public async Task<ApiServiceResponse<int>> DeleteBookAuthors(string bookId, List<string> authorIds, bool removeAll = false)
        {
            var result = new ApiServiceResponse<int>(false, Constants.UnableDeleteAuthors);
            result.ResponseObject = 0;
            if (string.IsNullOrEmpty(bookId))
            {
                return result;
            }
            if (removeAll)
            {
                var allBookAuthors = _bookAuthorRepository.GetAllAuthorIdsByBookId(bookId);
                await _bookAuthorRepository.DeleteBookAuthorsByBookId(bookId);
                result.Succeed = true;
                result.Errors = string.Empty;
                return result;
            }
            if (authorIds != null && authorIds.Count > 0)
            {
                var bookAuthorList = new List<BookAuthor>();
                foreach (var authorId in authorIds)
                {
                    var bookAuthor = await _bookAuthorRepository.GetBookAuthorByIds(bookId, authorId);
                    if (bookAuthor != null)
                    {
                        bookAuthorList.Add(bookAuthor);
                    }
                }
                if (bookAuthorList.Count > 0)
                {
                    await _bookAuthorRepository.Delete(bookAuthorList);
                }
                result.Succeed = true;
                result.ResponseObject = 1;
                result.Errors = string.Empty;
            }
            return result;
        }

        public async Task<ApiServiceResponse<List<BookView>>> FindBooks(string title = "", int totalPages = -1, int rating = -1, string year = "")
        {
            var result = new ApiServiceResponse<List<BookView>>(false, Constants.UnableFindBooks);
            var allBooks = await _bookRepository.GetAllBooks();
            if (allBooks == null)
            {
                return result;
            }
            if (string.IsNullOrEmpty(title) && allBooks.Count > 0)
            {
                var sortedcCollection = allBooks.Where(x => x.Title == title).ToList();
                allBooks = sortedcCollection;
            }
            if (totalPages > 0 && allBooks.Count > 0)
            {
                var sortedcCollection = allBooks.Where(x => x.TotalPages == totalPages).ToList();
                allBooks = sortedcCollection;
            }
            if (rating >= 0 && allBooks.Count > 0)
            {
                var sortedcCollection = allBooks.Where(x => x.Rating == rating).ToList();
                allBooks = sortedcCollection;
            }
            if (string.IsNullOrEmpty(year) && allBooks.Count > 0)
            {
                var sortedcCollection = allBooks.Where(x => x.YearOfPublishing == year).ToList();
                allBooks = sortedcCollection;
            }
            if (allBooks != null)
            {
                var finishBookCollection = new List<BookView>();
                foreach (var book in allBooks)
                {
                    var bookView = new BookView(book.Title, book.TotalPages, book.Rating, book.YearOfPublishing);
                    bookView.Id = book.Id;
                    finishBookCollection.Add(bookView);
                }
                result.Succeed = true;
                result.Errors = string.Empty;
                result.ResponseObject = finishBookCollection;
            }
            return result;
        }

        private async Task<bool> FindBook(BookView bookView)
        {
            var result = false;
            if (bookView == null)
            {
                return result;
            }
            var searchResult = await _bookRepository.FindBookByParams(bookView.Title, bookView.TotalPages, bookView.Rating, bookView.YearOfPublishing);
            if (searchResult != null)
            {
                result = true;
            }
            return result;
        }

        public async Task<ApiServiceResponse<BookView>> UpdateBook(BookView bookView, List<string> authorIds = null)
        {
            var result = new ApiServiceResponse<BookView>(false, Constants.UnableUpdateBook);
            var book = await _bookRepository.GetBookById(bookView.Id);
            if (book == null)
            {
                return result;
            }
            var bookUpdateResult = await _bookRepository.UpdateBook(book);
            if (bookUpdateResult == null || bookUpdateResult.State != Microsoft.EntityFrameworkCore.EntityState.Modified)
            {
                result.ResponseObject = null;
                return result;
            }
            result.Errors = string.Empty;
            if (authorIds != null && authorIds.Count > 0)
            {
                var bookAuthorDeleteResult = await DeleteBookAuthors(book.Id, authorIds, true);
                var bookAuthorAddResult = await AddBookAuthors(book.Id, authorIds);
                result.Errors = bookAuthorDeleteResult.Succeed && bookAuthorAddResult.Succeed ? string.Empty : Constants.UnableUpdateAuthors;
            }

            bookView.Id = book.Id;
            result.Succeed = true;
            result.ResponseObject = bookView;
            return result;
        }

        public async Task<ApiServiceResponse<int>> DeleteBook(string bookId)
        {
            var result = new ApiServiceResponse<int>(false, Constants.UnableRemoveBook);
            var book = await _bookRepository.GetBookById(bookId);
            if (book == null)
            {
                result.Errors = Constants.UnableFindBooks;
                return result;
            }
            var bookDeleteResult = await _bookRepository.DeleteBook(book);
            if (bookDeleteResult == null || bookDeleteResult.State != Microsoft.EntityFrameworkCore.EntityState.Deleted)
            {
                result.ResponseObject = 0;
                return result;
            }
            result.Errors = string.Empty;

            var bookAuthorDeleteResult = await DeleteBookAuthors(book.Id, null, true);
            result.Errors = bookAuthorDeleteResult.Succeed  ? string.Empty : Constants.UnableDeleteAuthors;

            result.Succeed = true;
            result.ResponseObject = 1;
            return result;
        }    
    }
}
