﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BookShop.Entities;
using BookShop.Services;
using BookShop.Services.Common;
using BookShop.ViewModels.ResponseViews;
using BookShop.ViewModels.User;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BookShop.API.Controllers
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly AccountService _accountService;
        private readonly RegularUserService _regularUserService;

        public AccountController(AccountService accountService, RegularUserService regularUserService, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _accountService = accountService;
            _regularUserService = regularUserService;
        }

        [HttpGet]
        [Route("test")]
        public string Test()
        {
            try
            {
                throw new Exception("Api is working!");
            }
            catch (Exception ex)
            {
                ;
            }
            return "Api is working!";
        }

        //...api/account/isEmailExist?email=test@gmail.com
        [HttpGet]
        [Route("isEmailExist")]
        public async Task<IActionResult> IsEmailExist(string email)
        {
            var response = new ApiServiceResponse<bool>();
            try
            {
                response.Succeed = true;
                response.Errors = string.Empty;
                response.ResponseObject = await _accountService.IsEmailExist(email);
                return Ok(response);
            }
            catch (Exception e)
            {
                response.ResponseObject = false;
                return BadRequest(response);
            }
        }

        //...api/account/token
        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> Login([FromBody] LoginView model)
        {
            var response = new ApiJsonResponse(false, "");
            try
            {
                Microsoft.AspNetCore.Identity.SignInResult result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
                if (result.Succeeded)
                {
                    response = await _accountService.Login(model);
                    if (response.Succeed)
                    {
                        //test
                        var user = User;
                        return Ok(response);
                    }
                    return BadRequest(result);
                }

                response.Succeed = false;
                response.Errors = Constants.BadRequestMessage;
                return BadRequest(response);
            }
            catch (Exception e)
            {
                response.Succeed = false;
                response.Errors = Constants.BadRequestMessage;
                response.ResponseString = string.Empty;
                return BadRequest(response);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        [Route("getApplicationUser")]
        public async Task<IActionResult> GetApplicationUser()
        {
            var response = new ApiServiceResponse<ApplicationUserView>();
            try
            {
                var id = User.Claims.FirstOrDefault(x => x.Type == "Id")?.Value;
                if (string.IsNullOrEmpty(id))
                {
                    response.Succeed = false;
                    response.Errors = Constants.PermissionerrorMessage;
                    response.ResponseObject = null;
                    return BadRequest(response);
                }
                response = await _accountService.GetApplicationUser(id);
                if (!response.Succeed)
                {
                    return BadRequest(response);
                }
                return Ok(response);
            }
            catch (Exception e)
            {
                response.Succeed = false;
                response.ResponseObject = null;
                response.Errors = Constants.BadRequestMessage;
                return BadRequest(response);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            var response = new ApiJsonResponse();
            try
            {
                var currentUserId = User.Claims.Where(x => x.Type == "Id").Select(x => x.Value).FirstOrDefault();
                response.Succeed = true;
                return Ok(response);
            }
            catch (Exception e)
            {
                response.Succeed = false;
                response.Errors = Constants.BadRequestMessage;
                return BadRequest(response);
            }
        }

        //...api/account/register
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterView model)
        {
            var response = new ApiJsonResponse();
            try
            {
                RegisterResponseAccountView result = await _accountService.RegisterUser(model);
              
                if (result != null && result.Messages.Count == 0)
                {
                    response.ResponseString = JsonConvert.SerializeObject(result);
                    await _signInManager.SignInAsync(result.User, false);
                    response.Succeed = true;
                    return Ok(response);
                }
                response.Succeed = false;
                response.Errors = "There is not result.";
                return BadRequest(response);
            }
            catch (Exception e)
            {
                response.Succeed = false;
                response.Errors = Constants.BadRequestMessage;
                response.ResponseString = string.Empty;
                return BadRequest(response);
            }
        }

    }
}