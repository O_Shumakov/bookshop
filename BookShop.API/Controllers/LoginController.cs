﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BookShop.DataAccess.AppContext;
using BookShop.Entities;
using BookShop.ViewModels.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BookShop.API.Controllers
{
    //[Route("api/login")]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        ApplicationContext db;
        public LoginController(ApplicationContext context, UserManager<ApplicationUser> manager, RoleManager<ApplicationRole> roleManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = manager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            this.db = context;
            //if (!db.Users.Any())
            //{
            //    db.Users.Add(new ApplicationUser { FirstName = "Tom", LastName = "Jones", Age = "26", Password = "123", Email = "tom@gmail.com", Gender = Entities.Enums.Gender.Male });
            //    db.Users.Add(new ApplicationUser { FirstName = "Alice", LastName = "Couper", Age = "29", Password = "123", Email = "alice@gmail.com", Gender = Entities.Enums.Gender.Male });
            //    db.Users.Add(new ApplicationUser { FirstName = "Kim", LastName = "Lee", Age = "21", Password = "123", Email = "kim@gmail.com", Gender = Entities.Enums.Gender.Female });
            //    db.Users.Add(new ApplicationUser { FirstName = "Jen", LastName = "Ien", Age = "45", Password = "123", Email = "jen@gmail.com", Gender = Entities.Enums.Gender.Other });
            //    db.Users.Add(new ApplicationUser { FirstName = "Stev", LastName = "Krup", Age = "33", Password = "123", Email = "stev@gmail.com", Gender = Entities.Enums.Gender.Male });
            //    db.SaveChanges();
            //}
        }

        public void GetRoles()
        {
            var roles = _roleManager.Roles.ToList();
        }

        [HttpGet]
        public IEnumerable<ApplicationUser> Get()
        {
            GetRoles();
            return db.Users.ToList();
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            ApplicationUser user = db.Users.FirstOrDefault(x => x.Id == id.ToString());
            if (user == null)
                return NotFound();
            return new ObjectResult(user);
        }

        // POST api/users
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ApplicationUser user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            db.Users.Add(user);
            db.SaveChanges();
            return Ok(user);
        }

        // PUT api/users/
        [HttpPut]
        public IActionResult Put([FromBody]ApplicationUser user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            if (!db.Users.Any(x => x.Id == user.Id))
            {
                return NotFound();
            }

            db.Update(user);
            db.SaveChanges();
            return Ok(user);
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            ApplicationUser user = db.Users.FirstOrDefault(x => x.Id == id.ToString());
            if (user == null)
            {
                return NotFound();
            }
            db.Users.Remove(user);
            db.SaveChanges();
            return Ok(user);
        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginView loginModel)
        {
            if (loginModel == null)
            {
                return null;
            }
            var user = await _userManager.FindByEmailAsync(loginModel.Email);
            if (user != null && user.Password == loginModel.Password)
            {
                await _signInManager.SignInAsync(user, false);
                return Ok(user);
            }

            return StatusCode((int)HttpStatusCode.BadRequest, user);
        }

        [Route("registr")]
        [HttpPost]
        public async Task<IActionResult> Registr([FromBody]ApplicationUser user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            user.UserName = $"{user.FirstName}{user.LastName}";
            var result = await _userManager.CreateAsync(user, user.Password);
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                return Ok(user);
            }

            return StatusCode((int)HttpStatusCode.BadRequest, user);
        }
    }
}