﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookShop.Services;
using BookShop.ViewModels.ResponseViews;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly BookService _bookService;
        private readonly RegularUserService _regularUserService;

        public BookController(BookService bookService, RegularUserService regularUserService)
        {
            _bookService = bookService;
            _regularUserService = regularUserService;
        }

        [HttpGet]
        [Route("getAllBooks")]
        public string GetAllBooks()
        {
            var response = new ApiJsonResponse(false, "");
            return string.Empty;
        }
    }
}