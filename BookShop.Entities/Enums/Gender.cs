﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.Entities.Enums
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}
