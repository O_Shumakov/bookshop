﻿using BookShop.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookShop.Entities
{
    public class Journal : BasePublication
    {
        public string Publisher { get; set; }

        public Journal()
        {
        }

        public Journal(string title, int pages, int rating, string publisher)
        {
            Title = title;
            TotalPages = pages;
            Rating = rating;
            Publisher = publisher;
        }
    }
}
