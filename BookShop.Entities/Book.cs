﻿using BookShop.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookShop.Entities
{
    public class Book : BasePublication
    {
        public string YearOfPublishing { get; set; }

        public Book()
        {
        }

        public Book(string title, int pages, int rating, string year)
        {
            Title = title;
            TotalPages = pages;
            Rating = rating;
            YearOfPublishing = year;
        }
    }
}
