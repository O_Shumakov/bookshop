﻿using BookShop.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookShop.Entities
{
    public class UserBook : BaseEntity
    {
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [ForeignKey("Book")]
        public string BookId { get; set; }
        public virtual Book Book { get; set; }

        public UserBook()
        {
        }

        public UserBook(string userId, string bookId)
        {
            UserId = userId;
            BookId = bookId;
        }
    }
}
