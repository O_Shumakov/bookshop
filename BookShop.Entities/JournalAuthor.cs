﻿using BookShop.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookShop.Entities
{
    public class JournalAuthor : BaseEntity
    {
        [ForeignKey("Author")]
        public string AuthorId { get; set; }
        public virtual Author Author { get; set; }

        [ForeignKey("Journal")]
        public string JournalId { get; set; }
        public virtual Journal Journal { get; set; }

        public JournalAuthor()
        {
        }

        public JournalAuthor(string journalId, string authorId)
        {
            JournalId = journalId;
            AuthorId = authorId;
        }
    }
}
