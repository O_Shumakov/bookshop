﻿using BookShop.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookShop.Entities
{
    public class UserJournal : BaseEntity
    {
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [ForeignKey("Journal")]
        public string JournalId { get; set; }
        public virtual Journal Journal { get; set; }

        public UserJournal()
        {
        }

        public UserJournal(string userId, string journalId)
        {
            UserId = userId;
            JournalId = journalId;
        }
    }
}
