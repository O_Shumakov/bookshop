﻿using BookShop.Entities.Base;
using BookShop.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BookShop.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string Age { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }
        public Gender Gender { get; set; }

        public string FullName => $"{this.FirstName} {this.LastName}";

        public ApplicationUser()
        {

        }

        public ApplicationUser(string email, string firstName, string lastName, Gender gender, string password = "")
        {
            Email = email;
            FirstName = firstName;
            LastName = lastName;
            Password = password;
            Gender = gender;
        }
    }
}
