﻿using BookShop.Entities.Base;
using BookShop.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookShop.Entities
{
    public class Author : BaseEntity
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }
        public Gender Gender { get; set; }

        public string FullName => $"{this.FirstName} {this.LastName}";

        public Author()
        {

        }

        public Author(string email, string firstName, string lastName, Gender gender)
        {
            Email = email;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
        }
    }
}
