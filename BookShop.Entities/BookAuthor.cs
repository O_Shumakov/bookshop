﻿using BookShop.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookShop.Entities
{
    public class BookAuthor : BaseEntity
    {
        [ForeignKey("Author")]
        public string AuthorId { get; set; }
        public virtual Author Author { get; set; }

        [ForeignKey("Book")]
        public string BookId { get; set; }
        public virtual Book Book { get; set; }

        public BookAuthor()
        {
        }

        public BookAuthor(string bookId, string authorId)
        {
            BookId = bookId;
            AuthorId = authorId;
        }
    }
}
