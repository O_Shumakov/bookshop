﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookShop.Entities.Base
{
    public class BasePublication : BaseEntity
    {
        public string Title { get; set; }
        public int TotalPages { get; set; }
        public int Rating { get; set; }
    }
}
