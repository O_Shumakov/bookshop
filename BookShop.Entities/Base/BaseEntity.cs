﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.Entities.Base
{
    public class BaseEntity
    {
        private string id;
        public string Id
        {
            get
            {
                return id ?? (id = Guid.NewGuid().ToString());
            }
            set
            {
                id = value;
            }
        }
    }
}
