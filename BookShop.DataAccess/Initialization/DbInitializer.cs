﻿using BookShop.DataAccess.AppContext;
using BookShop.Entities;
using BookShop.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Initialization
{
    public class DbInitializer
    {
        private readonly ApplicationContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public DbInitializer(ApplicationContext context, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public void Initialize()
        {
            var authorId = string.Empty;
            var bookId = string.Empty;
            var journalId = string.Empty;
            var userId = string.Empty;

            if (!_roleManager.Roles.Any(x => x.Name == "Admin"))
            {
                SeedAdminAndRoles();
            }
            if (!_roleManager.Roles.Any(x => x.Name == "User"))
            {
                userId = SeedUsers();
                bookId = SeedBooks();
                journalId = SeedJournals();
                authorId = SeedAuthors();
                SeedBookAuthors(bookId, authorId);
                SeedJournalAuthors(journalId, authorId);
                SeedUserBooks(userId, bookId);
                SeedUserJournals(userId, journalId);
            }
        }

        private void SeedAdminAndRoles()
        {
            var adminRole = new ApplicationRole("Admin");
            adminRole.RoleType = RoleType.Admin;
            _roleManager.CreateAsync(adminRole).GetAwaiter().GetResult();
            var email = "admin@admin.com";
            var password = "Qwerty!234567";
            var applicationUser = new ApplicationUser(email, "Admin", "Admin", Gender.Other, password);
            applicationUser.UserName = email;
            applicationUser.EmailConfirmed = true;
            _userManager.CreateAsync(applicationUser, password).GetAwaiter().GetResult();
            var user = _userManager.FindByNameAsync(email).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(user, "Admin").GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        private string SeedUsers()
        {
            var userRole = new ApplicationRole("User");
            userRole.RoleType = RoleType.User;
            _roleManager.CreateAsync(userRole).GetAwaiter().GetResult();
            var regularUser = new RegularUser();
            var userEmail = "testuser@gmail.com";
            var userPassword = "Qwe123@";
            var user = new ApplicationUser(userEmail, "Alex", "Hud", Gender.Male, userPassword);
            user.UserName = userEmail;
            user.EmailConfirmed = true;
            user.PhoneNumber = "123456789";
            user.Id = Guid.NewGuid().ToString();
            _userManager.CreateAsync(user, userPassword).GetAwaiter().GetResult();
            user = _userManager.FindByNameAsync(userEmail).GetAwaiter().GetResult();
            _userManager.AddToRoleAsync(user, "User").GetAwaiter().GetResult();
            regularUser.User = user;
            regularUser.UserId = user.Id;
            regularUser.Id = Guid.NewGuid().ToString();
            _context.RegularUsers.AddAsync(regularUser).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
            return user.Id;
        }

        private string SeedAuthors()
        {
            var author = new Author("testauthor@gmail.com", "Tom", "Rio", Gender.Male);
            author.Id = Guid.NewGuid().ToString();
            _context.Authors.AddAsync(author).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
            return author.Id;
        }

        private string SeedBooks()
        {
            var book = new Book("Test book", 250, 8, "1990");
            book.Id = Guid.NewGuid().ToString();
            _context.Books.AddAsync(book).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
            return book.Id;
        }

        private string SeedJournals()
        {
            var journal = new Journal("Test journal", 22, 6, "Pravda");
            journal.Id = Guid.NewGuid().ToString();
            _context.Journals.AddAsync(journal).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
            return journal.Id;
        }

        private void SeedBookAuthors(string bookId, string authorId)
        {
            var bookAuthor = new BookAuthor(bookId, authorId);
            _context.BookAuthors.AddAsync(bookAuthor).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        private void SeedJournalAuthors(string journalId, string authorId)
        {
            var journalAuthor = new JournalAuthor(journalId, authorId);
            _context.JournalAuthors.AddAsync(journalAuthor).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        private void SeedUserBooks(string userId, string bookId)
        {
            var userBook = new UserBook(userId, bookId);
            _context.UserBooks.AddAsync(userBook).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }

        private void SeedUserJournals(string userId, string journalId)
        {
            var userJournal = new UserJournal(userId, journalId);
            _context.UserJournals.AddAsync(userJournal).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
        }
    }
}
