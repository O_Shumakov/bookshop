﻿using BookShop.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookShop.DataAccess.AppContext
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            //Database.Migrate();
        }

        public DbSet<RegularUser> RegularUsers { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Journal> Journals { get; set; }
        public DbSet<UserBook> UserBooks { get; set; }
        public DbSet<UserJournal> UserJournals { get; set; }
        public DbSet<BookAuthor> BookAuthors { get; set; }
        public DbSet<JournalAuthor> JournalAuthors { get; set; }
    }
}
