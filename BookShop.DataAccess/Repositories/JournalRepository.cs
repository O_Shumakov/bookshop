﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories.Base;
using BookShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class JournalRepository : BaseRepository<Journal>
    {
        public JournalRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<Journal>> GetAllJournals()
        {
            var journals = await GetAll().ToListAsync();
            return journals;
        }

        public async Task<Journal> GetJournalById(string bookId)
        {
            var journal = await GetAll().FirstOrDefaultAsync(x => x.Id == bookId);
            return journal;
        }

        public async Task<List<Journal>> GetJournalsByPublisher(string publisher)
        {
            var journals = await GetAll().Where(x => x.Publisher == publisher).ToListAsync();
            return journals;
        }

        public async Task<List<Journal>> GetJournalsByRating(int rating)
        {
            var journals = await GetAll().Where(x => x.Rating == rating).ToListAsync();
            return journals;
        }

        public async Task<Journal> FindJournalByParams(string title, int pages, int rating, string publisher)
        {
            var journal = await GetAll().Where(x => x.Title == title && x.TotalPages == pages && x.Rating == rating && x.Publisher == publisher).FirstOrDefaultAsync();
            return journal;
        }

        public async Task<EntityEntry<Journal>> UpdateJournal(Journal journal)
        {
            if (journal == null)
            {
                return null;
            }
            var result = await Update(journal.Id, journal);
            return result;
        }

        public async Task<EntityEntry<Journal>> DeleteJournal(Journal journal)
        {
            if (journal == null)
            {
                return null;
            }
            var result = await Delete(journal.Id);
            return result;
        }
    }
}
