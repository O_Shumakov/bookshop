﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories.Base;
using BookShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class UserBookRepository : BaseRepository<UserBook>
    {
        public UserBookRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<UserBook>> GetAllUserBooks()
        {
            var userBooks = await GetAll().Include(x => x.User).Include(x => x.Book).ToListAsync();
            return userBooks;
        }

        public async Task<UserBook> GetUserBookById(string userId, string bookId)
        {
            var userBook = await GetAll().Include(x => x.User).Include(x => x.Book).Where(x => x.BookId == bookId && x.UserId == userId).FirstOrDefaultAsync();
            return userBook;
        }

        public async Task<List<UserBook>> GetUserBooksByBookId(string bookId)
        {
            var userBooks = await GetAll().Include(x => x.User).Include(x => x.Book).Where(x => x.BookId == bookId).ToListAsync();
            return userBooks;
        }

        public async Task<List<UserBook>> GetUserBooksByUserId(string userId)
        {
            var userBooks = await GetAll().Include(x => x.User).Include(x => x.Book).Include(x => x.User).Include(x => x.Book).Where(x => x.UserId == userId).ToListAsync();
            return userBooks;
        }

        public async Task<List<string>> GetAllBookIdsByUserId(string userId)
        {
            var bookIds = await GetAll().Include(x => x.User).Include(x => x.Book).Where(x => x.UserId == userId).Select(y => y.BookId).ToListAsync();
            return bookIds;
        }

        public async Task<List<Book>> GetAllBooksByUserId(string userId)
        {
            var books = await GetAll().Include(x => x.User).Include(x => x.Book).Where(x => x.UserId == userId).Select(y => y.Book).ToListAsync();
            return books;
        }

        public async Task<List<string>> GetAllUserIdsByBookId(string bookId)
        {
            var userIds = await GetAll().Include(x => x.User).Include(x => x.Book).Where(x => x.BookId == bookId).Select(y => y.UserId).ToListAsync();
            return userIds;
        }

        public async Task<List<ApplicationUser>> GetAllUsersByBookId(string bookId)
        {
            var users = await GetAll().Include(x => x.User).Include(x => x.Book).Where(x => x.BookId == bookId).Select(y => y.User).ToListAsync();
            return users;
        }

        public async Task DeleteUserBooksByUserId(string userId)
        {
            var userBooks = await GetUserBooksByUserId(userId);
            if (userBooks == null)
            {
                return;
            }
            await Delete(userBooks);
        }

        public async Task DeleteUserBooksByBookId(string bookId)
        {
            var userBooks = await GetUserBooksByBookId(bookId);
            if (userBooks == null)
            {
                return;
            }
            await Delete(userBooks);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<EntityEntry<UserBook>> DeleteUserBookByUserId(string userId, string bookId)
        {
            var userBook = await GetUserBookById(userId, bookId);
            if (userBook == null)
            {
                return null;
            }
            var result = await Delete(userBook.Id);
            return result;
        }
    }
}
