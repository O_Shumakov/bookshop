﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories.Base;
using BookShop.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class JournalAuthorRepository : BaseRepository<JournalAuthor>
    {
        public JournalAuthorRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<JournalAuthor>> GetAllJournalAuthors()
        {
            var journalAuthors = await GetAll().Include(x => x.Author).Include(x => x.Journal).ToListAsync();
            return journalAuthors;
        }

        public async Task<JournalAuthor> GetJournalAuthorByIds(string journalId, string authorId)
        {
            var journalAuthor = await GetAll().Include(x => x.Author).Include(x => x.Journal).Where(x => x.JournalId == journalId && x.AuthorId == authorId).FirstOrDefaultAsync();
            return journalAuthor;
        }

        public async Task<List<JournalAuthor>> GetJournalAuthorsByJournalId(string journalId)
        {
            var journalAuthors = await GetAll().Include(x => x.Author).Include(x => x.Journal).Where(x => x.JournalId == journalId).ToListAsync();
            return journalAuthors;
        }

        public async Task<List<JournalAuthor>> GetJournalAuthorsByAuthorId(string authorId)
        {
            var journalAuthors = await GetAll().Include(x => x.Author).Include(x => x.Journal).Where(x => x.AuthorId == authorId).ToListAsync();
            return journalAuthors;
        }

        public async Task<List<string>> GetAllJournalIdsByAuthorId(string authorId)
        {
            var journalIds = await GetAll().Include(x => x.Author).Include(x => x.Journal).Where(x => x.AuthorId == authorId).Select(y => y.JournalId).ToListAsync();
            return journalIds;
        }

        public async Task<List<Journal>> GetAllJournalsByAuthorId(string authorId)
        {
            var journals = await GetAll().Include(x => x.Author).Include(x => x.Journal).Where(x => x.AuthorId == authorId).Select(y => y.Journal).ToListAsync();
            return journals;
        }

        public async Task<List<string>> GetAllAuthorIdsByJournalId(string journalId)
        {
            var authorIds = await GetAll().Include(x => x.Author).Include(x => x.Journal).Where(x => x.JournalId == journalId).Select(y => y.AuthorId).ToListAsync();
            return authorIds;
        }

        public async Task<List<Author>> GetAllAuthorsByJournalId(string journalId)
        {
            var authors = await GetAll().Include(x => x.Author).Include(x => x.Journal).Where(x => x.JournalId == journalId).Select(y => y.Author).ToListAsync();
            return authors;
        }

        public async Task DeleteJournalAuthorsByAuthorId(string authorId)
        {
            var journalAuthors = await GetJournalAuthorsByAuthorId(authorId);
            if (journalAuthors == null)
            {
                return;
            }
            await Delete(journalAuthors);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteJournalAuthorsByJournalId(string journalId)
        {
            var journalAuthors = await GetJournalAuthorsByJournalId(journalId);
            if (journalAuthors == null)
            {
                return;
            }
            await Delete(journalAuthors);
            await _dbContext.SaveChangesAsync();
        }
    }
}
