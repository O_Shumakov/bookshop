﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories.Base;
using BookShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class AuthorRepository : BaseRepository<Author>
    {
        public AuthorRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<Author>> GetAllAuthors()
        {
            var users = await GetAll().ToListAsync();
            return users;
        }

        public async Task<Author> GetAuthorById(string id)
        {
            var author = await GetAll().FirstOrDefaultAsync(x => x.Id == id);
            return author;
        }

        public async Task<List<Author>> GetAuthorsById(List<string> authorsId)
        {
            List<Author> filteredItems = new List<Author>();
            foreach (var authorId in authorsId)
            {
                var author = await GetAuthorById(authorId);
                if (author != null)
                {
                    filteredItems.Add(author);
                }
            }
            return filteredItems;
        }

        public async Task<EntityEntry<Author>> UpdateAuthor(Author author)
        {
            if (author == null)
            {
                return null;
            }
            var result = await Update(author.Id, author);
            return result;
        }

        public async Task<EntityEntry<Author>> DeleteAuthor(string authorId)
        {
            if (string.IsNullOrEmpty(authorId))
            {
                return null;
            }
            var result = await Delete(authorId);
            return result;
        }
    }
}
