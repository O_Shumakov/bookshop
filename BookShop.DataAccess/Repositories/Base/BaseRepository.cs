﻿using BookShop.DataAccess.AppContext;
using BookShop.Entities.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories.Base
{
    public class BaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ApplicationContext _dbContext;

        public BaseRepository(ApplicationContext context)
        {
            _dbContext = context;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().AsNoTracking();
        }

        public async Task<TEntity> GetById(string id)
        {
            return await _dbContext.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<EntityEntry<TEntity>> Create(TEntity entity)
        {
            DeleteTrackedEntities();
            var result = await _dbContext.Set<TEntity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return result;
        }

        public async Task Create(IEnumerable<TEntity> entities)
        {
            DeleteTrackedEntities();
            await _dbContext.Set<TEntity>().AddRangeAsync(entities);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<EntityEntry<TEntity>> Update(string id, TEntity entity)
        {
            DeleteTrackedEntities();
            var result = _dbContext.Set<TEntity>().Update(entity);
            await _dbContext.SaveChangesAsync();
            return result;
        }

        public async Task Update(IEnumerable<TEntity> entity)
        {
            DeleteTrackedEntities();
            _dbContext.Set<TEntity>().UpdateRange(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<EntityEntry<TEntity>> Delete(string id)
        {
            DeleteTrackedEntities();
            var entity = await GetById(id);
            var result = _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
            return result;
        }

        public async Task Delete(IEnumerable<TEntity> entity)
        {
            DeleteTrackedEntities();
            _dbContext.Set<TEntity>().RemoveRange(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteByIds(List<string> ids)
        {
            if (ids.Count == 0)
            {
                return;
            }

            DeleteTrackedEntities();
            foreach (var id in ids)
            {
                await Delete(id);
            }
        }

        private void DeleteTrackedEntities()
        {
            var changedEntriesCopy = _dbContext.ChangeTracker.Entries().ToList();
            foreach (var entity in changedEntriesCopy)
            {
                _dbContext.Entry(entity.Entity).State = EntityState.Detached;
            }
        }

        public async Task<int> Count()
        {
            return await _dbContext.Set<TEntity>().AsNoTracking().CountAsync();
        }

        public virtual async Task SaveChanges()
        {
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw ex;
            }
        }
    }
}
