﻿using BookShop.DataAccess.AppContext;
using BookShop.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class AccountRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountRepository(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<ApplicationUser> GetByEmail(string email)
        {
            var result = await _userManager.Users.FirstOrDefaultAsync(x => x.Email == email);
            return result;
        }

        public async Task<ApplicationUser> GetUserById(string id)
        {
            var result = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        public async Task<List<string>> GetRoles(ApplicationUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            return roles.ToList();
        }

        public async Task<IdentityResult> AddToRole(ApplicationUser user, string role)
        {
            var result = await _userManager.AddToRoleAsync(user, role);
            return result;
        }

        public async Task<IdentityResult> CreateUser(ApplicationUser user)
        {
            IdentityResult result = await _userManager.CreateAsync(user);
            return result;
        }

        public async Task<IdentityResult> CreateUserWithPassword(ApplicationUser user, string password)
        {
            IdentityResult result = await _userManager.CreateAsync(user, password);
            return result;
        }
    }
}
