﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories.Base;
using BookShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class UserJournalRepository : BaseRepository<UserJournal>
    {
        public UserJournalRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<UserJournal>> GetAllUserJournals()
        {
            var userJournals = await GetAll().Include(x => x.User).Include(x => x.Journal).ToListAsync();
            return userJournals;
        }

        public async Task<UserJournal> GetUserJournalById(string userId, string journalId)
        {
            var userJournal = await GetAll().Include(x => x.User).Include(x => x.Journal).Where(x => x.JournalId == journalId && x.UserId == userId).FirstOrDefaultAsync();
            return userJournal;
        }

        public async Task<List<UserJournal>> GetUserJournalsByJournalId(string journalId)
        {
            var userJournals = await GetAll().Include(x => x.User).Include(x => x.Journal).Where(x => x.JournalId == journalId).ToListAsync();
            return userJournals;
        }

        public async Task<List<UserJournal>> GetUserJournalsByUserId(string userId)
        {
            var userJournals = await GetAll().Include(x => x.User).Include(x => x.Journal).Where(x => x.UserId == userId).ToListAsync();
            return userJournals;
        }

        public async Task<List<string>> GetAllJournalIdsByUserId(string userId)
        {
            var journalIds = await GetAll().Include(x => x.User).Include(x => x.Journal).Where(x => x.UserId == userId).Select(y => y.JournalId).ToListAsync();
            return journalIds;
        }

        public async Task<List<Journal>> GetAllJournalsByUserId(string userId)
        {
            var journals = await GetAll().Include(x => x.User).Include(x => x.Journal).Where(x => x.UserId == userId).Select(y => y.Journal).ToListAsync();
            return journals;
        }

        public async Task<List<string>> GetAllUserIdsByJournalId(string journalId)
        {
            var userIds = await GetAll().Include(x => x.User).Include(x => x.Journal).Where(x => x.JournalId == journalId).Select(y => y.UserId).ToListAsync();
            return userIds;
        }

        public async Task<List<ApplicationUser>> GetAllUsersByJournalId(string journalId)
        {
            var users = await GetAll().Include(x => x.User).Include(x => x.Journal).Where(x => x.JournalId == journalId).Select(y => y.User).ToListAsync();
            return users;
        }

        public async Task DeleteUserJournalsByUserId(string userId)
        {
            var userJournals = await GetUserJournalsByUserId(userId);
            if (userJournals == null)
            {
                return;
            }
            await Delete(userJournals);
        }

        public async Task DeleteUserJournalsByJournalId(string journalId)
        {
            var userJournals = await GetUserJournalsByJournalId(journalId);
            if (userJournals == null)
            {
                return;
            }
            await Delete(userJournals);
        }

        public async Task<EntityEntry<UserJournal>> DeleteUserJournalByUserId(string userId, string journalId)
        {
            var userJournal = await GetUserJournalById(userId, journalId);
            if (userJournal == null)
            {
                return null;
            }
            var result = await Delete(userJournal.Id);
            return result;
        }
    }
}
