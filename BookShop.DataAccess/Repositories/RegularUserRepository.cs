﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories.Base;
using BookShop.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class RegularUserRepository : BaseRepository<RegularUser>
    {
        public RegularUserRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<RegularUser>> GetAllUsers()
        {
            var users = await GetAll().Include(x => x.User).ToListAsync();
            return users;
        }

        public async Task<RegularUser> GetUserByUserId(string userId)
        {
            var user = await GetAll().Include(x => x.User).FirstOrDefaultAsync(x => x.UserId == userId);
            return user;
        }

        public async Task UpdateUser(RegularUser user)
        {
            var normolizeEmail = user.User.Email.ToUpper();
            user.User.NormalizedEmail = normolizeEmail;
            user.User.UserName = user.User.Email;
            user.User.NormalizedUserName = normolizeEmail;
            await Update(user.Id, user);
        }

        public async Task Delete(string userId)
        {
            var user = await GetUserByUserId(userId);
            if (user == null)
            {
                return;
            }
            //_dbContext.Set<ApplicationUser>().Remove(user.User);
            _dbContext.Set<RegularUser>().Remove(user);
            await _dbContext.SaveChangesAsync();
        }
    }
}
