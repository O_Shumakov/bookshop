﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories.Base;
using BookShop.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class BookAuthorRepository : BaseRepository<BookAuthor>
    {
        public BookAuthorRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<BookAuthor>> GetAllBookAuthors()
        {
            var bookAuthors = await GetAll().Include(x => x.Author).Include(x => x.Book).ToListAsync();
            return bookAuthors;
        }

        public async Task<BookAuthor> GetBookAuthorByIds(string bookId, string authorId)
        {
            var bookAuthor = await GetAll().Include(x => x.Author).Include(x => x.Book).Where(x => x.BookId == bookId && x.AuthorId == authorId).FirstOrDefaultAsync();
            return bookAuthor;
        }

        public async Task<List<BookAuthor>> GetBookAuthorsByBookId(string bookId)
        {
            var bookAuthors = await GetAll().Include(x => x.Author).Include(x => x.Book).Where(x => x.BookId == bookId).ToListAsync();
            return bookAuthors;
        }

        public async Task<List<BookAuthor>> GetBookAuthorsByAuthorId(string authorId)
        {
            var bookAuthors = await GetAll().Include(x => x.Author).Include(x => x.Book).Where(x => x.AuthorId == authorId).ToListAsync();
            return bookAuthors;
        }

        public async Task<List<string>> GetAllBookIdsByAuthorId(string authorId)
        {
            var bookIds = await GetAll().Include(x => x.Author).Include(x => x.Book).Where(x => x.AuthorId == authorId).Select(y => y.BookId).ToListAsync();
            return bookIds;
        }

        public async Task<List<Book>> GetAllBooksByAuthorId(string authorId)
        {
            var books = await GetAll().Include(x => x.Author).Include(x => x.Book).Where(x => x.AuthorId == authorId).Select(y => y.Book).ToListAsync();
            return books;
        }

        public async Task<List<string>> GetAllAuthorIdsByBookId(string bookId)
        {
            var authorIds = await GetAll().Include(x => x.Author).Include(x => x.Book).Where(x => x.BookId == bookId).Select(y => y.AuthorId).ToListAsync();
            return authorIds;
        }

        public async Task<List<Author>> GetAllAuthorsByBookId(string bookId)
        {
            var authors = await GetAll().Include(x => x.Author).Include(x => x.Book).Where(x => x.BookId == bookId).Select(y => y.Author).ToListAsync();
            return authors;
        }

        public async Task DeleteBookAuthorsByAuthorId(string authorId)
        {
            var bookAuthors = await GetBookAuthorsByAuthorId(authorId);
            if (bookAuthors == null)
            {
                return;
            }
            await Delete(bookAuthors);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteBookAuthorsByBookId(string bookId)
        {
            var bookAuthors = await GetBookAuthorsByBookId(bookId);
            if (bookAuthors == null)
            {
                return;
            }
            await Delete(bookAuthors);
            await _dbContext.SaveChangesAsync();
        }
    }
}
