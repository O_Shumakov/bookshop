﻿using BookShop.DataAccess.AppContext;
using BookShop.DataAccess.Repositories.Base;
using BookShop.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShop.DataAccess.Repositories
{
    public class BookRepository : BaseRepository<Book>
    {
        public BookRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<List<Book>> GetAllBooks()
        {
            var books = await GetAll().ToListAsync();
            return books;
        }

        public async Task<Book> GetBookById(string bookId)
        {
            var book = await GetAll().FirstOrDefaultAsync(x => x.Id == bookId);
            return book;
        }

        public async Task<List<Book>> GetBooksByYear(string year)
        {
            var books = await GetAll().Where(x => x.YearOfPublishing == year).ToListAsync();
            return books;
        }

        public async Task<List<Book>> GetBooksByRating(int rating)
        {
            var books = await GetAll().Where(x => x.Rating == rating).ToListAsync();
            return books;
        }

        public async Task<Book> FindBookByParams(string title, int pages, int rating, string year)
        {
            var book = await GetAll().Where(x => x.Title == title && x.TotalPages == pages && x.Rating == rating && x.YearOfPublishing == year).FirstOrDefaultAsync();
            return book;
        }

        public async Task<EntityEntry<Book>> UpdateBook(Book book)
        {
            var result = await Update(book.Id, book);
            return result;
        }

        public async Task<EntityEntry<Book>> DeleteBook(Book book)
        {
            if (book == null)
            {
                return null;
            }
            var result = await Delete(book.Id);
            return result;
        }
    }
}
